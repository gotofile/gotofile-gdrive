# GoTo files for Drive

App to create, edit, and view GoTo files from Google Drive.

## CLI Commands

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production (note: assumes that the base URL is /gotofile-gdrive/)
npm run build
```

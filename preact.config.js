export default function (config, env, helpers) {
  // if production then base URL is /gotofile-gdrive/ (from https://gotofile.gitlab.io/gotofile-gdrive/)
  // the trailing / is necessary
  const BASE_URL = env.production ? '/gotofile-gdrive/' : ''
  config.output.publicPath = BASE_URL
  
  // if this isn't done, rmwc styles do not get added to the bundle in production
  config.optimization['sideEffects'] = false;
}
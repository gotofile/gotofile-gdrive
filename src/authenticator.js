import { v4 as uuidv4 } from 'uuid';

// OAuth handler

export default class Authenticator {
  // when the client id is needed, it is pulled from the querystring of this url, therefore it can be hardcoded here
  static endpoint = new URL('https://accounts.google.com/o/oauth2/v2/auth?client_id=969587357926-nhrtbqrqtjvl2skis5og6olvu9bgrfle.apps.googleusercontent.com&response_type=token&scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fdrive.file');

  static doLogin(driveState, prompt) {
    // create a copy of the current url that can be used as an oauth redirect uri (no querystring and no fragment)
    var redirectableURL = new URL(window.location.toString());
    redirectableURL.search = '';
    redirectableURL.hash = '';

    // add information to the authentication endpoint
    var newloc = Object.assign(this.endpoint);
    newloc.searchParams.set('redirect_uri', redirectableURL.toString());
    newloc.searchParams.set('state', uuidv4());
    newloc.searchParams.set('login_hint', driveState.userId);
    if (typeof prompt == 'string') {
      newloc.searchParams.set('prompt', prompt);
    }

    // save data and redirect
    window.sessionStorage.setItem(newloc.searchParams.get('state'), JSON.stringify(driveState));
    window.location = newloc.toString();
  }
}
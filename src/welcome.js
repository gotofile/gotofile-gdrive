import { Component } from 'preact';

import './styles/welcome.css';

export default class Welcome extends Component {
	render(props) {
		return (
			<div className="welcome">
				<h1>{props.i18n.t('welcome.title')}</h1>
				<p>{props.i18n.t('welcome.text')}</p>
				<button className="action" onClick={() => {
					window.open('https://drive.google.com')
				}}>{props.i18n.t('welcome.openDrive')}</button>
				<p>{props.i18n.t('welcome.installInvitation.text')}</p>
				<button onClick={() => {
					window.open('https://gsuite.google.com/marketplace/app/goto_files/969587357926')
				}}>{props.i18n.t('welcome.installInvitation.button')}</button>
				<h2>{props.i18n.t('welcome.createHelp.title')}</h2>
				<ol>
					<li>{props.i18n.t('welcome.createHelp.step0')}</li>
					<li>{props.i18n.t('welcome.createHelp.step1')}</li>
					<li>{props.i18n.t('welcome.createHelp.step2')}</li>
					<li>{props.i18n.t('welcome.createHelp.step3')}</li>
					<li>{props.i18n.t('welcome.createHelp.step4')}</li>
					<li>{props.i18n.t('welcome.createHelp.step5')}</li>
				</ol>
				<h2>{props.i18n.t('welcome.editHelp.title')}</h2>
				<h3>{props.i18n.t('welcome.editHelp.isDefault.title')}</h3>
				<ol>
					<li>{props.i18n.t('welcome.editHelp.isDefault.step1')}</li>
					<li>{props.i18n.t('welcome.editHelp.isDefault.step2')}</li>
				</ol>
				<h3>{props.i18n.t('welcome.editHelp.notDefault.title')}</h3>
				<ol>
					<li>{props.i18n.t('welcome.editHelp.notDefault.step0')}</li>
					<li>{props.i18n.t('welcome.editHelp.notDefault.step1')}</li>
					<li>{props.i18n.t('welcome.editHelp.notDefault.step2')}</li>
					<li>{props.i18n.t('welcome.editHelp.notDefault.step3')}</li>
					<li>{props.i18n.t('welcome.editHelp.notDefault.step4')}</li>
				</ol>
				<h2>{props.i18n.t('welcome.defaultHelp.title')}</h2>
				<p>{props.i18n.t('welcome.defaultHelp.text')}</p>
				<ol>
					<li>{props.i18n.t('welcome.defaultHelp.step0')}</li>
					<li>{props.i18n.t('welcome.defaultHelp.step1')}</li>
					<li>{props.i18n.t('welcome.defaultHelp.step2')}</li>
					<li>{props.i18n.t('welcome.defaultHelp.step3')}</li>
					<li>{props.i18n.t('welcome.defaultHelp.step4')}</li>
				</ol>
			</div>
		);
	}
}

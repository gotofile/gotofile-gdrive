// convert files between decoded INI inside .url files and decoded JSON inside GoTo v1 files

export default class ShortcutConverter {
  static fromGoto(gotoSource) {
    var output = {};
    if (gotoSource.version === 'shortcut') {
      // the file contains the old parsed content, therefore it should be modified and not recreated from scratch
      if (typeof gotoSource.source !== 'object') throw 'Invalid format';
      output = Object.assign(gotoSource.source);
      var hasSetInternetShortcut = false;
      for (var key in output) {
        var lowercaseKey = key.toLowerCase();
        // remove [DEFAULT] and [DOC#...] sections
        if (lowercaseKey === 'default' || lowercaseKey.startsWith('doc')) {
          delete output[key];
        } else if (lowercaseKey === 'internetshortcut') {
          hasSetInternetShortcut = true;
          if (typeof output[key] !== 'object') output[key] = {};
          var hasSetUrl = false;
          for (var innerKey in output[key]) {
            var lowercaseInnerKey = innerKey.toLowerCase();
            // remove last edit date from the output file
            if (lowercaseInnerKey === 'modified') delete output[key][innerKey];
            else if (lowercaseInnerKey === 'url') {
              hasSetUrl = true;
              output[key][innerKey] = gotoSource.url;
            }
          }
          // add the url if it hasn't already been added (there was no value in the [InternetShortcut] section named "URL" case insensitive)
          if (!hasSetUrl) {
            output[key].URL = gotoSource.url;
          }
        }
      }
      // add the [InternetShortcut] if it doesn't already exist
      if (!hasSetInternetShortcut) {
        output.InternetShortcut = {
          URL: gotoSource.url
        };
      }
    } else if (gotoSource.version === 1) {
      // the file does not contain old parsed content
      output = {
        InternetShortcut: {
          URL: gotoSource.url
        }
      }
    } else {
      throw 'Invalid format';
    }
    return output;
  }

  // if useCustomFormat === false the output will be a valid GoTo v1 scheme, otherwise "version" === "shortcut" and the output will contain the original decoded INI source
  static toGoto(iniSource, useCustomFormat) {
    var output = {
      version: useCustomFormat ? 'shortcut' : 1
    };
    if (useCustomFormat) {
      output.source = Object.assign(iniSource);
    }
    for (var key in iniSource) {
      var lowercaseKey = key.toLowerCase();
      if (lowercaseKey === 'internetshortcut') {
        for (var innerKey in iniSource[key]) {
          var lowercaseInnerKey = innerKey.toLowerCase();
          if (lowercaseInnerKey === 'url') {
            output.url = iniSource[key][innerKey];
          }
        }
      }
    }
    if (typeof output.url !== 'string') {
      throw 'Invalid format';
    }
    return output;
  }
}
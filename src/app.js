import { Component, Fragment } from 'preact';

import { CircularProgress } from '@rmwc/circular-progress';
import '@rmwc/circular-progress/dist/styles';
import { DialogQueue, createDialogQueue  } from '@rmwc/dialog';
import '@rmwc/dialog/dist/styles';
import ini from 'ini';

import i18n from "./i18n/i18n";
import Authenticator from './authenticator';
import ShortcutConverter from './shortcut-converter';
import Welcome from './welcome';
import './styles/app.css';

const defaultContent = { // content for an empty file
	version: 1,
	url: '',
	notes: null
};

export default class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loggedIn: false,
			loading: true,
			autoGo: false,
			fileMeta: {},
			status: '',
			statusError: false,
			saving: false,
			edited: false,
			editable: false,
			settings: false
		};

		this._dialogQueuer = createDialogQueue();

		if (typeof window != 'undefined') {
			// actually in the browser

			// create a URL object from the window.location string to be able to access the query string as a URLSearchParams object
			var currentURL = new URL(window.location.toString());

			if (currentURL.hash.startsWith('#')) {
				var fragmentParams = new URLSearchParams(currentURL.hash.substr(1));
			} else {
				var fragmentParams = new URLSearchParams(currentURL.hash);
			}

			if (
				currentURL.searchParams.get('state') != null
			) {
				// user is coming from the google drive file open flow
				// parse the google drive state
				try {
					var driveState = JSON.parse(currentURL.searchParams.get('state'));
				} catch (err) {
					// invalid query parameters, user doesn't come from the drive file open flow
					this.state.loggedIn = false;
					this.state.loading = false;
					return;
				}

				Authenticator.doLogin(driveState, 'none');
			} else if (fragmentParams.get('state') != null) {
				// user is returning from the login flow
				try {
					var driveState = JSON.parse(window.sessionStorage.getItem(fragmentParams.get('state')));
					if (driveState == null) throw 'driveState cannot be null';
				} catch (err) {
					this._dialogQueuer.alert({
						title: i18n.t('errors.invalidData.title'),
						body: i18n.t('errors.invalidData.text'),
						acceptLabel: 'OK'
					});
					this.state.loading = false;
					this.state.loggedIn = false;
					return;
				}

				window.sessionStorage.removeItem(fragmentParams.get('state'));

				if (fragmentParams.get('error') != null) {
					if (fragmentParams.get('error') == 'interaction_required') {
						// user must be prompted for consent: log in again, this time without prompt=none
						Authenticator.doLogin(driveState, null);
						return;
					} else {
						this._dialogQueuer.alert({
							title: i18n.t('errors.dialogTitle'),
							body: fragmentParams.get('error'),
							acceptLabel: 'OK'
						});
						return;
					}
				}

				this.state.authHeaders = new Headers({
					'Authorization': fragmentParams.get('token_type') + ' ' + fragmentParams.get('access_token')
				});
				this.state.driveState = driveState;
				this.state.loggedIn = true;
				this.state.loading = true;

				if (driveState.action == 'open') {
					// restore the state in the URL bar to allow page reloads
					var newURL = new URL(currentURL);
					newURL.hash = '';
					newURL.searchParams.set('state', JSON.stringify(driveState));
					history.replaceState({}, document.title, newURL.href.substr(newURL.origin.length));

					this.state.fileId = driveState.ids[0];
					this._loadFile();
				} else if (driveState.action == 'create') {
					var formData  = new FormData();
					formData.append('metadata', new Blob(
						[JSON.stringify({
							name: i18n.t('untitledFilename'),
							mimeType: 'application/goto+json',
							parents: [
								this.state.driveState.folderId
							]
						})],
						{ type: 'application/json' }
					));
					var content = Object.assign(defaultContent);
					if (content.notes == null) content.notes = undefined;
					formData.append('file', new Blob(
						[JSON.stringify(content)],
						{ type: 'application/goto+json' }
					));
					fetch('https://www.googleapis.com/upload/drive/v3/files?supportsAllDrives=true&uploadType=multipart', {
						method: 'POST',
						headers: this.state.authHeaders,
						body: formData,
						cache: 'no-cache'
					}).then((response) => {
						if (response.status == 200) {
							response.json().then((jsonResp) => {
								this.setState({
									loading: false,
									loggedIn: true,
									editable: true,
									fileMeta: jsonResp,
									fileId: jsonResp.id,
									content: content,
									status: i18n.t('statuses.saved', {
										date: this._timeFormatter.format(new Date())
									}),
									statusError: false
								});
								// create a state in the URL bar that points to "load the newly created file" to allow page reloads
								var newURL = new URL(currentURL);
								newURL.hash = '';
								newURL.searchParams.set('state', JSON.stringify({
									ids: [
										jsonResp.id
									],
									action: 'open',
									userId: driveState.userId
								}));
								history.replaceState({}, document.title, newURL.href.substr(newURL.origin.length));
							}).catch((err) => {
								console.error(err);
								this._dialogQueuer.alert({
									title: i18n.t('errors.dialogTitle'),
									body: i18n.t('statuses.saveError'),
									preventOutsideDismiss: true,
									acceptLabel: null
								});
							});
						} else {
							console.error('Invalid status code', response.status, response);
							this._dialogQueuer.alert({
								title: i18n.t('errors.dialogTitle'),
								body: i18n.t('statuses.saveError'),
								preventOutsideDismiss: true,
								acceptLabel: null
							});
						}
					}).catch((err) => {
						console.error(err);
						this._dialogQueuer.alert({
							title: i18n.t('errors.dialogTitle'),
							body: i18n.t('statuses.saveError'),
							preventOutsideDismiss: true,
							acceptLabel: null
						});
					});
				}
			} else {
				this.state.loading = false;
				this.state.loggedIn = false;
			}
		} else {
			// set variables for the prerenderer (show the loader)
			this.state.loading = true;
		}
	}

	_timeFormatter = new Intl.DateTimeFormat(undefined, {
		weekday: 'short',
		year: 'numeric',
		month: 'long',
		day: 'numeric',
		hour: '2-digit',
		minute: '2-digit',
		second: '2-digit'
	})

	_loadFile = () => {
		return new Promise((resolve, reject) => {
			var promptReauthentication = () => {
				var promise = this._dialogQueuer.alert({
					title: i18n.t('errors.loadErrorPermissionMaybeRequired.title'),
					body: i18n.t('errors.loadErrorPermissionMaybeRequired.text'),
					acceptLabel: i18n.t('errors.loadErrorPermissionMaybeRequired.grantButton'),
					preventOutsideDismiss: true
				});
				promise.then(() => {
					this.setState({
						loading: true
					}, () => {
						Authenticator.doLogin(this.state.driveState, null);
					});
				});
				return promise;
			}
			var filePromises = [];
			filePromises.push(fetch('https://www.googleapis.com/drive/v3/files/' + encodeURIComponent(this.state.driveState.ids[0]) + '?supportsAllDrives=true&alt=media', {
				method: 'GET',
				headers: this.state.authHeaders,
				cache: 'no-cache'
			}));
			filePromises.push(fetch('https://www.googleapis.com/drive/v3/files/' + encodeURIComponent(this.state.driveState.ids[0]) + '?supportsAllDrives=true&fields=name,modifiedTime,lastModifyingUser(displayName,emailAddress),capabilities(canEdit,canModifyContent)', {
				method: 'GET',
				headers: this.state.authHeaders,
				cache: 'no-cache'
			}));
			Promise.all(filePromises).then((responses) => {
				for (var response of responses) {
					if (response.status == 403) {
						// error may be caused by downloads being forbidden. check for that and display a different error if that is the case.
						return response.json().then((errData) => {
							var isErrorCausedByDownloadForbidden = false;
							if (
								typeof errData.error == 'object'
								&& typeof errData.error.errors == 'object'
								&& Array.isArray(errData.error.errors)
							) {
								for (var error of errData.error.errors) {
									if (
										typeof error.reason == 'string'
										&& error.reason == 'cannotDownloadFile'
									) {
										isErrorCausedByDownloadForbidden = true;
									}
								}
							}
							if (isErrorCausedByDownloadForbidden) {
								console.error('Downloading file is forbidden, cannot load');
								this._dialogQueuer.alert({
									title: i18n.t('errors.loadErrorDownloadForbidden.title'),
									body: i18n.t('errors.loadErrorDownloadForbidden.text'),
									preventOutsideDismiss: true,
									acceptLabel: null
								});
								reject('Downloading file is forbidden, cannot load');
							} else {
								console.error(errData);
								promptReauthentication();
								reject(errData);
							}
						}).catch((err) => {
							console.error(err);
							promptReauthentication();
							reject(err);
						});
					} else if (response.status != 200) {
						console.error('Invalid response status code:', response.status, response);
						promptReauthentication();
						return reject({
							description: 'Invalid response status code',
							response: response
						});
					}
				}
				responses[1].json().then((fileMeta) => {
					this.setState({
						fileMeta: fileMeta,
						fileId: this.state.driveState.ids[0],
						editable: fileMeta.capabilities.canEdit && fileMeta.capabilities.canModifyContent,
						status: i18n.t('statuses.lastEdited', {
							name: fileMeta.lastModifyingUser.displayName,
							email: fileMeta.lastModifyingUser.emailAddress,
							date: this._timeFormatter.format(new Date(fileMeta.modifiedTime))
						}),
						statusError: false
					}, () => {
						responses[0].text().then((text) => {
							var fileContent = null;

							// attempt to parse as JSON
							try {
								fileContent = JSON.parse(text);
							} catch (err) {
								fileContent = null;

								// attempt to parse as INI shortcut
								try {
									fileContent = ShortcutConverter.toGoto(ini.parse(text), true);
								} catch (err) {
									fileContent = null;
								}
							}

							if (fileContent === null) {
								this._dialogQueuer.alert({
									title: i18n.t('errors.invalidContent.title'),
									body: i18n.t('errors.invalidContent.text'),
									acceptLabel: 'OK'
								});
								this.setState({
									loading: false,
									content: defaultContent
								}, () => { resolve() });
							} else {
								// if fileContent !== null then the file was parsed succesfully
								if (fileContent.version !== 1 && fileContent.version !== 'shortcut') {
									console.error('Unsupported file version:', fileContent.version);
									this._dialogQueuer.alert({
										title: i18n.t('errors.unsupportedVersion.title'),
										body: i18n.t('errors.unsupportedVersion.text'),
										preventOutsideDismiss: true,
										acceptLabel: null
									});
									return reject({
										description: 'Unsupported file version',
										version: fileContent.version
									});
								}
								var autoGo = false, autoGoSetting;
								if (
									// autoGo trigger conditions:
									// file must not have notes
									typeof fileContent.notes !== 'string' &&
									// url must be valid
									this._checkURL(fileContent.url) &&
									// url must use http or https scheme
									(
										fileContent.url.toLowerCase().startsWith('http://') ||
										fileContent.url.toLowerCase().startsWith('https://')
									) &&
									// autogo setting must exist and be > 2
									!isNaN(autoGoSetting = parseInt(window.localStorage.getItem('gotofile-gdrive-autogo'))) &&
									autoGoSetting > 2
								) {
									autoGo = new Date(new Date().getTime() + (autoGoSetting * 1000));
								}
								this.setState({
									loading: false,
									autoGo,
									content: fileContent
								}, () => { resolve() });
							}
						}).catch((err) => {
							console.error(err);
							this._dialogQueuer.alert({
								title: i18n.t('errors.invalidContent.title'),
								body: i18n.t('errors.invalidContent.text'),
								acceptLabel: 'OK'
							});
							this.setState({
								loading: false,
								content: defaultContent
							}, () => { resolve() });
						});
					})
				}).catch((err) => {
					console.error(err);
					promptReauthentication();
					reject(err);
				});
			}).catch((err) => {
				console.error(err);
				promptReauthentication();
				reject(err);
			});
		});
	}

	componentDidMount() {
		document.addEventListener('keydown', this._onKeyDown);
	}

	componentWillUnmount() {
		document.removeEventListener('keydown', this._onKeyDown);
	}

	_onKeyDown = (e) => {
		if (this.state.autoGo !== false && this.state.autoGo !== null) {
			if (e.key === 'Enter') {
				e.preventDefault();
				this._triggerAutoGo();
			} else if (e.key === 'Shift') {
				e.preventDefault();
				this._cancelAutoGo();
			}
		}
	}

	_triggerAutoGo = () => {
		if (this.state.autoGo === false || this.state.autoGo === null) return;
		this.setState({
			autoGo: null
		}, () => window.location.href = this.state.content.url);
	}

	_cancelAutoGo = () => {
		if (this.state.autoGo === false || this.state.autoGo === null) return;
		this.setState({
			autoGo: false
		});
	}

	_renderAutoGo = () => {
		// display information about autoGo if active
		if (this.state.autoGo !== false) {
			// set a timeout to rerender to update the time
			setTimeout(() => {
				this.forceUpdate();
			}, 50);
			if (this.state.autoGo !== null && this.state.autoGo < new Date()) {
				this._triggerAutoGo();
			}
			return (
				<div className="single-view-centered">
					<div className="auto-go">
						<h1>{this.state.autoGo === null ? i18n.t('autoGo.loading') : i18n.t('autoGo.countdown', { count: Math.round((this.state.autoGo.getTime() - new Date().getTime()) / 1000) })}</h1>
						<input
							type="url"
							id="url"
							required
							value={this.state.content.url}
							readOnly={true}
						/>
						<div>
							<div>
								<button
									class="action"
									disabled={this.state.autoGo === null}
									onClick={this._triggerAutoGo}
								>{i18n.t('autoGo.goButton.text')}</button>
								<span className="gray">{i18n.t('autoGo.goButton.subtext')}</span>
							</div>
							<div>
								<button
									disabled={this.state.autoGo === null}
									onClick={this._cancelAutoGo}
								>{i18n.t('autoGo.editButton.text')}</button>
								<span className="gray">{i18n.t('autoGo.editButton.subtext')}</span>
							</div>
						</div>
					</div>
				</div>
			);
		} else {
			return (<Fragment />);
		}
	}

	_renderSettings = () => {
		if (this.state.settings) {
			let autoGoSetting = parseInt(window.localStorage.getItem('gotofile-gdrive-autogo'));
			if (isNaN(autoGoSetting) || autoGoSetting < 3) autoGoSetting = null;
			return (
				<Fragment>
					<div class="button-group">
						<input type="checkbox" id="settings-enable-autogo" checked={typeof autoGoSetting === 'number'} />
						<label for="settings-enable-autogo">{i18n.t('settings.autoGo.beforeInput')}<input type="number" value={typeof autoGoSetting === 'number' ? autoGoSetting : 3} min="3" id="settings-autogo" style="width:80px" />{i18n.t('settings.autoGo.afterInput')}</label>
					</div>
					<div class="button-group">
						<button
							class="action"
							onClick={() => {
								if (document.getElementById('settings-enable-autogo').checked) {
									window.localStorage.setItem('gotofile-gdrive-autogo', document.getElementById('settings-autogo').value);
								} else {
									window.localStorage.removeItem('gotofile-gdrive-autogo');
								}
								this.setState({
									settings: false
								});
							}}
						>{i18n.t('settings.actions.save')}</button>
						<button
							onClick={() => {
								this.setState({
									settings: false
								});
							}}
						>{i18n.t('settings.actions.cancel')}</button>
					</div>
				</Fragment>
			);
		} else {
			return (<Fragment />);
		}
	}

	_renderWelcome = () => {
		// display a welcome screen if nothing is loading and the editor wasn't opened from drive
		if (!this.state.loading && !this.state.loggedIn) {
			return (<Welcome i18n={i18n} />);
		}
	}

	_renderLoading = () => {
		// display a loading icon, only if something is currently loading
		if (this.state.loading) {
			return (
				<div className="single-view-centered">
					<CircularProgress size="xlarge" />
				</div>
			);
		} else {
			return (<Fragment />);
		}
	}

	_checkURL = (url) => {
		// try to convert the string to a URL object. if it fails, then the string is not a valid URL
		try {
			new URL(url);
			return true;
		} catch (err) {
			return false;
		}
	}

	_changeTimeout = null

	_createFieldChangeCallback = (valueName) => {
		return (e) => {
			var content = Object.assign(this.state.content) // create a copy of content
			content[valueName] = e.target.value;
			this.setState({
				content: content,
				edited: true
			}, () => {
				// wait 3.5 seconds since the last change before attemting to autosave
				if (this._changeTimeout != null) {
					window.clearTimeout(this._changeTimeout);
				}
				this._changeTimeout = setTimeout(() => {
					if (document.getElementById('save').disabled) return; // don't autosave if the file is currently invalid
					this._save();
				}, 3500);
			});
		}
	}

	_save = () => {
		if (this.state.saving || !this.state.editable) return;
		// clear any timeout that may provoke another save
		if (this._changeTimeout != null) {
			window.clearTimeout(this._changeTimeout);
		}

    var headers = Object.assign(this.state.authHeaders); // copy the headers, need to add content-type

    var body = '';
    if (this.state.content.version === 'shortcut') {
      // file should be saved as a URL file
      body = ini.stringify(ShortcutConverter.fromGoto(this.state.content));
      headers.set('Content-Type', 'application/internet-shortcut');
    } else if (this.state.content.version === 1) {
      // file should be saved as a GoTo file
      var content = Object.assign(this.state.content);
			if (content.notes == null) content.notes = undefined;
      body = JSON.stringify(this.state.content);
      headers.set('Content-Type', 'application/goto+json');
    } else {
      throw 'Invalid version';
    }

		this.setState({
			status: i18n.t('statuses.saveInProgress'),
			statusError: false,
			saving: true
		}, () => {
			fetch('https://www.googleapis.com/upload/drive/v3/files/' + encodeURIComponent(this.state.fileId) + '?supportsAllDrives=true&uploadType=media', {
				method: 'PATCH',
				headers: headers,
				body,
				cache: 'no-cache'
			}).then((response) => {
				if (response.status == 200) {
					this.setState({
						status: i18n.t('statuses.saved', {
							date: this._timeFormatter.format(new Date())
						}),
						statusError: false,
						saving: false
					});
				} else {
					console.error('Invalid status code', response.status, response);
					this.setState({
						status: i18n.t('statuses.saveError'),
						statusError: true,
						saving: false
					});
				}
			}).catch((err) => {
				console.error(err);
				this.setState({
					status: i18n.t('statuses.saveError'),
					statusError: true,
					saving: false
				});
			});
		});
	}

	_renderEditor = () => {
		// display a loading icon, only if nothing is currently loading and the editor was opened from drive
		if (!this.state.loading && !this.state.settings && (this.state.autoGo === false) && this.state.loggedIn) {
			var isUrlValid = this._checkURL(this.state.content.url);
			return (
				<Fragment>
					<div class="button-group">
						<button
						  id="save"
							class="action"
							disabled={(!isUrlValid || this.state.saving || !this.state.editable)}
							onClick={this._save}
						>{i18n.t('actions.save')}</button>
						<button
							disabled={!isUrlValid}
							onClick={() => {
								if (!isUrlValid) return;
								var open = () => {
									if (this.state.edited) {
										window.open(this.state.content.url);
									} else {
										location.href = this.state.content.url;
									}
								}
								var lowercase = this.state.content.url.toLowerCase();
								if (
									lowercase.startsWith('http://')
									|| lowercase.startsWith('https://')
								) {
									open();
								} else {
									this._dialogQueuer.confirm({
										title: i18n.t('unsafeSchemeWarning.title'),
										body: i18n.t('unsafeSchemeWarning.text'),
										acceptLabel: i18n.t('unsafeSchemeWarning.open'),
										cancelLabel: i18n.t('unsafeSchemeWarning.cancel'),
									}).then((response) => {
										if (response) open();
									});
								}
							}}
						>{i18n.t('actions.openURL')}</button>
						<span id="statusText" className={(this.state.statusError ? 'error' : 'gray')}>{this.state.status}</span>
						<button
							onClick={() => {
								this.setState({
									settings: true
								});
							}}
						>{i18n.t('actions.settings')}</button>
					</div>
					<div class="field-container form-group input-container">
						<label for="url">{i18n.t('fields.url')}</label>
						<input
							type="url"
							id="url"
							required
							value={(typeof this.state.content.url == 'string') ? this.state.content.url : ''}
							onChange={this._createFieldChangeCallback('url')}
							readOnly={!this.state.editable}
						/>
					</div>
					<div className="field-container form-group textarea-container">
						<label for="notes">{i18n.t('fields.notes')}</label>
  					<textarea
							id="notes"
							onChange={this._createFieldChangeCallback('notes')}
							readOnly={!this.state.editable || (typeof this.state.content === 'object' && this.state.content.version === 'shortcut')}
							disabled={typeof this.state.content === 'object' && this.state.content.version === 'shortcut'}
						>{(typeof this.state.content.notes == 'string') ? this.state.content.notes : ''}</textarea>
					</div>
				</Fragment>
			);
		} else {
			// editor is not being displayed => clear autosave timer, if any
			if (this._changeTimeout != null) {
				window.clearTimeout(this._changeTimeout);
				this._changeTimeout = null;
			}
		}
	}

	render() {
		if (typeof window != 'undefined') {
			window.document.title = (typeof this.state.fileMeta.name == 'string') ? this.state.fileMeta.name : i18n.t('appName');
		}
		return (
			<div id="app">
				<h1 id="filename">{this.state.settings ? i18n.t('settings.title') : ((typeof this.state.fileMeta.name == 'string') ? this.state.fileMeta.name : i18n.t('appName'))}</h1>
				<DialogQueue
					dialogs={this._dialogQueuer.dialogs}
					preventOutsideDismiss
				/>
				{this._renderLoading()}
				{this._renderSettings()}
				{this._renderAutoGo()}
				{this._renderEditor()}
				{this._renderWelcome()}
			</div>
		);
	}
}
